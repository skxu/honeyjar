import { Meteor } from 'meteor/meteor';

Meteor.publish('usersDirectory', function() {
	return Meteor.users.find({},
	{
		'emails.$': 1,
		'username': 1,
		'createdAt':1
	});
});