import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import UserSchema from './schema';
import UserProfileSchema from '/imports/api/profiles/schema';
import { Profiles } from '/imports/api/profiles/profiles';

Meteor.users.attachSchema(UserSchema);

Meteor.users.allow({
	insert: () => { return true; },
	update: () => { return true; },
	remove: () => { return false; }
});

Meteor.users.after.insert((userId, doc) => {
	if (Meteor.isServer) {
		let userProfile = {
			bio: "a short description of me",
			userId: doc._id,
			honeyjars: []
		};
		UserProfileSchema.clean(userProfile);
		check(userProfile, Profiles.simpleSchema());
		Profiles.insert(userProfile, (error, result) => {
			if (error) {
				console.log(error);
			}
		});
	}
	
});