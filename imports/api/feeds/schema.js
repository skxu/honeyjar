import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import LinkSchema from '/imports/api/links/schema';

export default new SimpleSchema({
	_id: {
		type: String
	},
	userId: {
		type: String,
		index: true
	},
	honeyJarId: {
		type: String,
		index: true
	},
	linkId: {
		type: String,
		index: true
	},
	link: {
		type: LinkSchema
	},
	createdAt: {
	    type: Date,
	    autoValue: function() {
	      if (this.isInsert) {
	        return new Date();
	      } else if (this.isUpsert) {
	        return {$setOnInsert: new Date()};
	      } else {
	        this.unset();  // Prevent user from supplying their own value
	      }
	    },
	    optional: true
	},
	read: {
		type: Boolean,
		defaultValue: false,
		index: true
	},
	saveForLater: {
		type: Boolean,
		defaultValue: false,
		index: true
	},
	favorite: {
		type: Boolean,
		defaultValue: false,
		index: true
	},
	metadata: {
		type: Object,
		optional: true
	}


});