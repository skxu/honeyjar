import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import FeedSchema from './schema';

export const Feeds = new Mongo.Collection('feeds');

Feeds.attachSchema(FeedSchema);

Feeds.allow({
	insert: () => { return true; },
	update: () => { return true; },
	remove: () => { return false; }
});
