import { Meteor } from 'meteor/meteor';
import { Feeds } from './feeds';
import { Links } from '/imports/api/links/links';

Meteor.methods({
	'feeds.toggleRead'(feedId, isRead) {
		return Feeds.update(
			{_id:feedId},
			{$set: {
				read: isRead
			}},
			function (error, result) {
				if (error) {
					console.log(error);
					throw new Meteor.Error('feeds.toggleRead.updateError', error);
				}
			}
		);
	}
});

Meteor.methods({
	'feeds.toggleFavorite'(feedId, isFavorite) {
		return Feeds.update(
			{_id:feedId},
			{$set: {
				favorite: isFavorite
			}},
			function (error, result) {
				if (error) {
					console.log(error);
					throw new Meteor.Error('feeds.toggleFavorite.updateError', error);
				}
			}
		);
	}
});

Meteor.methods({
	'feeds.toggleSave'(feedId, isSave) {
		return Feeds.update(
			{_id:feedId},
			{$set: {
				saveForLater: isSave
			}},
			function (error, result) {
				if (error) {
					console.log(error);
					throw new Meteor.Error('feeds.toggleSave.updateError', error);
				}
			}
		);
	}
});

Meteor.methods({
	'feeds.delete'(feed) {
		return Feeds.remove(
			{
				honeyJarId: feed.honeyJarId,
				linkId: feed.linkId
			},
			function (error) {
				if (error) {
					console.log(error);
					throw new Meteor.Error('feeds.delete', error);
				} else {
					Links.remove({_id: feed.linkId}, function(error) {
						if (error) {
							console.log(error);
							throw new Meteor.Error('feeds.delete', error);
						}
					});
				}
			}
		);
	}
})