import { Meteor } from 'meteor/meteor';
import { Feeds } from '../feeds.js';

Meteor.publish('feeds', function() {
	return Feeds.find({
		userId: this.userId
	});
});

Meteor.publish('feeds.honeyjar', function(honeyJarId) {
	return Feeds.find({
		userId: this.userId,
		honeyJarId: honeyJarId
	}, {
		sort: { createdAt: -1 }
	});
});