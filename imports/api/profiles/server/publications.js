// All links-related publications

import { Meteor } from 'meteor/meteor';
import { Profiles } from '/imports/api/profiles/profiles';

// Meteor.publish('links.all', function () {
//   return Links.find();
// });

Meteor.publish('profile', function() {
	return Profiles.find({
		userId: this.userId
	});
});

Meteor.publish('profiles', function() {
	return Profiles.find();
});