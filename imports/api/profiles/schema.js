import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import HoneyJarSchema from '/imports/api/honeyjars/schema';

export default new SimpleSchema({
    userId: {
        type: String,
        index: true,
        unique: true
    },
    firstName: {
        type: String,
        optional: true
    },
    lastName: {
        type: String,
        optional: true
    },
    birthday: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        allowedValues: ['Male', 'Female'],
        optional: true
    },
    organization : {
        type: String,
        optional: true
    },
    website: {
        type: String,
        regEx: SimpleSchema.RegEx.Url,
        optional: true
    },
    bio: {
        type: String,
        optional: true
    },
    country: {
        type: String,
        optional: true
    },

    "honeyjars": {
        type: [HoneyJarSchema],
        optional: true
    }
});