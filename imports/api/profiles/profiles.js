import { Meteor } from 'meteor/meteor';
import UserProfileSchema from './schema';

export const Profiles = new Mongo.Collection('profiles');

Profiles.attachSchema(UserProfileSchema);