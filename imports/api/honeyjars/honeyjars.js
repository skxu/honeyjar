import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Meteor } from 'meteor/meteor';
import HoneyJarSchema from './schema';
import UserSchema from '/imports/api/users/schema';
import { Profiles } from '/imports/api/profiles/profiles';

export const HoneyJars = new Mongo.Collection('honeyjars');

HoneyJars.attachSchema(HoneyJarSchema);