import { Meteor } from 'meteor/meteor';
import { HoneyJars } from '/imports/api/honeyjars/honeyjars';

Meteor.publish('honeyjars', function() {
	return HoneyJars.find({
		userId: this.userId
	});
});

Meteor.publish('honeyjar', function(id) {
	return HoneyJars.find({_id: id});
});