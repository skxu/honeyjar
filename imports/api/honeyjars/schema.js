import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import UserSchema from '/imports/api/users/schema';
import { Meteor } from 'meteor/meteor';

export default new SimpleSchema({
	"_id": {
		type: String
	},
  	"name": {
		type: String,
		label: "Name",
		max: 200,
		optional: true
	},
	"users": {
		type: [UserSchema],
		label: "Users"
	},
  	createdAt: {
	    type: Date,
	    autoValue: function() {
	      if (this.isInsert) {
	        return new Date();
	      } else if (this.isUpsert) {
	        return {$setOnInsert: new Date()};
	      } else {
	        this.unset();  // Prevent user from supplying their own value
	      }
	    },
	    optional: true
	}
});