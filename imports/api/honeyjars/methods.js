import { Meteor } from 'meteor/meteor';
import { HoneyJars } from '/imports/api/honeyjars/honeyjars';
import { Profiles } from '/imports/api/profiles/profiles';
import { Links } from '/imports/api/links/links';
import { Feeds } from '/imports/api/feeds/feeds';

Meteor.methods({
	'honeyjars.addUser'(honeyJar, userEmail) {
		let honeyJarId = honeyJar._id;
		let user = Meteor.users.findOne({"emails.0.address": userEmail});
		if (!user) {
			console.log('Could not find user with email: ' + userEmail);
			throw new Meteor.Error('honeyjars.addUser', 'user not found');
		}
		honeyJar.users.find((existingUser) => {
			if (user._id === existingUser._id) {
				// user exists
				console.log('user: ' + user.username + ' already is part of honeyjar.');
				throw new Meteor.Error('honeyjars.addUser', 'user already part of honeyjar');
			}
		}); 

		if (user) {
			HoneyJars.update(
				honeyJarId,
				{$push:{users: user}}
			);
			let userProfile = Profiles.findOne({userId: user._id});
			
			// upsert honeyjar
			let honeyJars = userProfile.honeyjars;
			let hasJar = false;
			honeyJars.map((jar) => {
				if (jar._id === honeyJar._id) {
					hasJar = true;
					return honeyJar;
				} else {
					return jar;
				}
			});
			if (!hasJar) {
				honeyJars.push(honeyJar);
			}

			let userProfileId = userProfile._id;
			Profiles.update(
				{ _id: userProfileId },
				{ $set: {honeyjars: honeyJars}},
				(error, numUpdated) => {
					if (error) {
						console.log(error);
						throw new Meteor.Error("honeyjar.addUser.profileUpdateError", "Error while updating profile");
					} else {
						console.log("Number of profiles updated: ", numUpdated);
					}
				}
			);
			let links = Links.find({honeyJarId: honeyJarId}).fetch();
			links.forEach((link) => {
				let feed = {
					userId: user._id,
					honeyJarId: honeyJarId,
					linkId: link._id,
					link: link
				}
				Feeds.insert(feed, (error, result) => {
					if (error) {
						console.log(error);
					} else {
						console.log("Feed updated: ", result);
					}
				});
			}); 

		} else {
			console.log("Could not find user with email: " + userEmail);
			throw new Meteor.Error("honeyjar.addUser.notFound", "Could not find user with email: " + userEmail);
		}
	},

	'honeyjars.insert'(honeyJar, userId) {
		console.log('honeyJar: ', honeyJar);
		console.log('userId: ', userId);
		HoneyJars.insert(honeyJar, function(error, result) {
			if (error) {
				console.log(error);
				throw new Meteor.Error("honeyjar.insert", "Could not insert honeyjar: " + error);
			}
			let userProfile = Profiles.findOne({userId: userId});
			let userProfileId = userProfile._id;
			
			
			// if profile has jar, update it, else push it
			let honeyJars = userProfile.honeyjars;
			let hasJar = false;
			honeyJars.map((jar) => {
				if (jar._id === honeyJar._id) {
					hasJar = true;
					return honeyJar;
				} else {
					return jar;
				}
			});
			if (!hasJar) {
				honeyJars.push(honeyJar);
			}


			return Profiles.update(
				{ _id: userProfileId },
				{ $set: {honeyjars: honeyJars}},
				(error, numUpdated) => {
					if (error) {
						console.log(error);
						throw new Meteor.Error("honeyjar.insert", "Could not update profile: " + error);
					} else {
						console.log("Number of profiles updated: ", numUpdated);
					}
				}
			);
		});
	}
})