// Definition of the links collection

import { Mongo } from 'meteor/mongo';
import LinkSchema from './schema';
import { HoneyJars } from '/imports/api/honeyjars/honeyjars';
import { Feeds } from '/imports/api/feeds/feeds';

export const Links = new Mongo.Collection('links');

Links.attachSchema(LinkSchema);

Links.allow({
	insert: () => { return true; },
	update: () => { return true; },
	remove: () => { return true; }
});


Links.after.insert((userId, doc) => {
	if (Meteor.isServer) {
		let honeyjar = HoneyJars.findOne({_id: doc.honeyJarId});
		let linkId = doc._id;
		honeyjar.users.forEach((user) => {
			let feed = {
				userId: user._id,
				honeyJarId: doc.honeyJarId,
				linkId: linkId,
				link: doc
			}
			Feeds.insert(feed, (error, result) => {
				if (error) {
					console.log(error);
				} else {
					console.log("Created feed with id: " + result);
				}
			});
		});
	}
});

Links.after.update((userId, doc) => {
	if (Meteor.isServer) {
		let honeyjar = HoneyJars.findOne({_id: doc.honeyJarId});
		let linkId = doc._id;
		honeyjar.users.forEach((user) => {
			Feeds.update(
				{linkId: linkId, userId:user._id}, 
				{$set: {
					link: doc
				}},
				(error, result) => {
				if (error) {
					console.log(error);
				} else {
					console.log("Updated " + result + " feeds");
				}
			});
		});
	}
});