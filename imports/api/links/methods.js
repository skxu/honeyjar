// Methods related to links

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Links } from './links.js';
import URI from 'urijs';
import unfluff from 'unfluff';
import request from 'request-promise-native';

Meteor.methods({
  'links.insert'(link) {
  	if (link.url && !/^(?:f|ht)tps?\:\/\//.test(link.url.toLowerCase())) {
        link.url = "http://" + link.url;
    }
    
    link.url = new URI(link.url).normalize().toString();

    let isUrl =	/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(link.url)
    if (!isUrl) {
    	console.log(link.url + " is not a valid URL");
    	throw new Meteor.Error("links.insert.invalidUrl", link.url + " is not a valid URL");
    }
    Links.simpleSchema().clean(link);
	return Links.insert(link, function(error, linkId) {
		if (error) {
			Meteor.Error(error);
		} else {
			request({
				url: link.url,
				headers: {
					'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'
				}
			})
		  		.then(function(body) {
		  			let data = unfluff(body);
		  			console.log(data);
		  			let pathArray = link.url.split('/');
		  			let baseUrl = pathArray[0] + '//' + pathArray[2];
		  			let description = (data.description) ? data.description : data.text;
		  			if (description.length > 400) description = description.substring(0, 400) + "...";
		  			let thumbnailUrl = null;
		  			if (data.image) {
		  				if (!/^(?:f|ht)tps?\:\/\//.test(data.image)) {
			        		if (data.image.length > 2 && data.image[0] == '/' && data.image[1] == '/') {
			    				// hacky fix for scraper fail
			    				thumbnailUrl = 'http:' + data.image;
			    			} else {
			        			thumbnailUrl = baseUrl + data.image;
			        		}
			    		} else {
		    				thumbnailUrl = data.image;
			    		}
			    	}
		  			Links.update(linkId, {$set: {
		  				description: description,
		  				title: data.title,
		  				thumbnailUrl: thumbnailUrl
		  			}});
		  		})
		  		.catch(function(err) {
		  			console.log(err);
		  			throw new Meteor.Error("links.insert.fetchError", err);
		  		});
		}
	});
  }
});
