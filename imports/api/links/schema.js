import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export default new SimpleSchema({
	honeyJarId: {
		type: String,
		index: true
	},
	url: {
		type: String
	},
	title: {
		type: String,
		optional: true
	},
	description: {
		type: String,
		optional: true
	},
	createdAt: {
	    type: Date,
	    autoValue: function() {
	      if (this.isInsert) {
	        return new Date();
	      } else if (this.isUpsert) {
	        return {$setOnInsert: new Date()};
	      } else {
	        this.unset();  // Prevent user from supplying their own value
	      }
	    },
	    optional: true
	},
	thumbnailUrl: {
		type: String,
		optional: true
	},
	tags: {
		type: [String],
		defaultValue: []
	},
	authorUserId: {
		type: String
	}
})