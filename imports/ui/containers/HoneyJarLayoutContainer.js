import React from 'react';
import { HoneyJarLayout } from '/imports/ui/layouts/HoneyJarLayout';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Feeds } from '/imports/api/feeds/feeds';
import { HoneyJars } from '/imports/api/honeyjars/honeyjars';
import { Profiles } from '/imports/api/profiles/profiles';
import { browserHistory } from 'react-router';

export default HoneyJarContainer = createContainer(({ params }) => {
	const user = Meteor.user();
	if (!user && !Meteor.loggingIn()) {
		browserHistory.push('/login');
	}
	let directoryHandle = Meteor.subscribe('usersDirectory');
	let profilesHandle = Meteor.subscribe('profiles');
	const { id } = params;
	
	let honeyJarHandle = (id) ? Meteor.subscribe('honeyjar', id) : null;
	let feedHandle = (id) ? Meteor.subscribe('feeds.honeyjar', id) : null;
	

	const userProfile = (user) ? Profiles.findOne({userId: user._id}) : null; 
	const loading = !directoryHandle.ready() || !profilesHandle.ready();
	const honeyJarLoading = (honeyJarHandle && !honeyJarHandle.ready()) 
		|| (feedHandle && !feedHandle.ready());
	
	const currentHoneyJar = (id) ? HoneyJars.findOne({_id: id}) : null;
	let feeds = [];
	if (user && id) {
		feeds = Feeds.find({honeyJarId: id, userId: user._id}, {sort: {createdAt: -1}}).fetch();	
	}

	return {
		loading,
		honeyJarLoading,
		user,
		userProfile,
		currentHoneyJar,
		feeds
	}
}, HoneyJarLayout);