import React from 'react';
import { HoneyJar } from '/imports/ui/components/HoneyJar';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Feeds } from '/imports/api/feeds/feeds';
import { HoneyJars } from '/imports/api/honeyjars/honeyjars';

export default HoneyJarContainer = createContainer(({ params }) => {
	const { id } = params;
	if (!id) {
		const loading = true;
		const honeyJar = null;
		const feeds = [];
		const user = Meteor.user();
		return {
			loading,
			honeyJar,
			feeds,
			user
		};
	}
	let feedHandle = Meteor.subscribe('feeds.honeyjar', id);
	let honeyjarHandle = Meteor.subscribe('honeyjar', id);
	let directoryHandle = Meteor.subscribe('usersDirectory');
	let profilesHandle = Meteor.subscribe('profiles');
	const loading = !feedHandle.ready() || !honeyjarHandle.ready() || !directoryHandle.ready() || !profilesHandle.ready();
	const user = Meteor.user();
	const honeyJar = HoneyJars.findOne({_id: id});
	let feeds = [];
	if (user) {
		feeds = Feeds.find({honeyJarId: id, userId: user._id}, {sort: {createdAt: -1}}).fetch();
	} 
	
	return {
		loading,
		honeyJar,
		feeds,
		user
	}
}, HoneyJar);