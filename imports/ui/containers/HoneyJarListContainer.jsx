import React from 'react';
import { HoneyJarList } from '/imports/ui/components/HoneyJarList';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { HoneyJars } from '/imports/api/honeyjars/honeyjars';
import { Profiles } from '/imports/api/profiles/profiles';

export default HoneyJarListContainer = createContainer(({ params }) => {
	const user = Meteor.user();
	let handle = Meteor.subscribe('profile');
	const loading = !handle.ready();
	const userProfile = (user) ? Profiles.findOne({userId: user._id}) : null; 

	return {
		loading,
		user,
		userProfile
	}
}, HoneyJarList);