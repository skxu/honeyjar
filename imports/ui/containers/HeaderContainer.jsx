import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Header } from '/imports/ui/components/header/Header';

export default HeaderContainer = createContainer(({ params }) => {
	const user = Meteor.user();
	const loggingIn = Meteor.loggingIn();
	return {
		user,
		loggingIn
	}
}, Header);  
