import React from 'react';
import { ShareLinkPage } from '/imports/ui/pages/ShareLinkPage';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Profiles } from '/imports/api/profiles/profiles';
import { browserHistory } from 'react-router';

export default HoneyJarContainer = createContainer(({ params }) => {
	
	let profilesHandle = Meteor.subscribe('profiles');
	const user = Meteor.user();
	const loading = !profilesHandle.ready() || Meteor.loggingIn();
	
	let honeyJars = [];
	if (!Meteor.loggingIn() && !user) {
		browserHistory.push('/login');
	}
	if (user && !loading) {
		const userProfile = Profiles.findOne({userId: user._id}); 
		honeyJars = userProfile.honeyjars;
	}
	
	return {
		loading,
		honeyJars,
		user
	}
}, ShareLinkPage);