import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { MainLayout } from '/imports/ui/layouts/MainLayout';

export default MainLayoutContainer = createContainer(({ params }) => {
	const loggedIn = Meteor.user() != null;
	const loggingIn = Meteor.loggingIn();
	if (loggedIn) {
		message = "Successfully logged in";
	} else if (loggingIn) {
		message = "Logging you in...";
	} else {
		message = "Logged out";
	}
	return {
		loggedIn,
		loggingIn,
		message
	}
}, MainLayout);  
