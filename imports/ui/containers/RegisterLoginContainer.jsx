import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { RegisterLoginPage } from '/imports/ui/pages/RegisterLoginPage';

export default RegisterLoginContainer = createContainer(({ params }) => {
	const loggedIn = Meteor.user() != null;
	const loggingIn = Meteor.loggingIn();
	const message = (loggedIn) ? "Successfully logged in" : "Logging you in..."; 
	const autoHideDuration = (loggedIn) ? 1500 : -1	
	return {
		loggedIn,
		loggingIn,
		message,
		autoHideDuration
	}
}, RegisterLoginPage);  
