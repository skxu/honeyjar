import React from 'react';
import { Tabs, Tab } from 'material-ui/Tabs';
import { Register } from '/imports/ui/components/register/Register';
import { Login } from '/imports/ui/components/login/Login';
import { Meteor } from 'meteor/meteor';




export class RegisterLoginPage extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const style = {
			width: 400,			
			position: "absolute",
			margin: "0 auto",
			bottom: "20%",
			left: 0,
			right: 0,
			maxWidth: "90%"
		}
			return (
				<div style={{ position:"relative", height:"100vh" }}>
					<div style={style}>
						{
							!this.props.loggedIn && 
							this.props.location.pathname == "/register" && 
							<Register 
								handleError={this.props.handleError}
								handleLogin={this.props.handleLogin}
							/> 
						}
						{
							!this.props.loggedIn &&
							this.props.location.pathname == "/login" && 
							<Login 
								handleError={this.props.handleError}
								handleLogin={this.props.handleLogin}
							/>
						}
					</div>
				</div>
			)
		
	}
}