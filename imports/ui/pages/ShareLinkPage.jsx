import React from 'react';

import Paper from 'material-ui/Paper';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';

import { browserHistory } from 'react-router';

export class ShareLinkPage extends React.Component {
	constructor(props) {
		super(props);
		const queryParams = this.props.location.query;

		this.state = {
			selectedHoneyJar: null,
			url: (queryParams && queryParams.url) ? queryParams.url : '',
			title: (queryParams && queryParams.title) ? queryParams.title: ''
		};
		this.handleSelectHoneyJar = this.handleSelectHoneyJar.bind(this);
		this.onChangeUrl = this.onChangeUrl.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSelectHoneyJar(event, index, value) {
		this.setState({
			selectedHoneyJar: value
		});
	}

	onChangeUrl(event) {
		this.setState({
			url: event.target.value
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.honeyJars.length > 0 
			&& this.state.selectedHoneyJar === null) {
			this.setState({
				selectedHoneyJar: nextProps.honeyJars[0]
			});
		}
	}

	handleSubmit() {
		let link = {
			honeyJarId: this.state.selectedHoneyJar._id,
			url: this.state.url,
			authorUserId: this.props.user._id,
			title: this.state.title
		};
		Meteor.call("links.insert", link, (error, result) => {
			if (error) {
				console.log(error);
				this.props.handleError(error);
			} else {
				console.log("added link: " + result);
				browserHistory.push('/honeyjars/' 
					+ this.state.selectedHoneyJar._id
					+ '?initialView=jar');
			}
		});
	}

	render() {

		let honeyJars = [];
		this.props.honeyJars.forEach((honeyJar, idx) => {
			honeyJars.push(
				<MenuItem 
					value={honeyJar}
					primaryText={honeyJar.name}
					key={idx}
				/>
			);
		});
		return(
			<div>
				<Paper
					style={{
						height:'calc(100vh - 0px)',
						padding: 40
					}}
				>
					{ this.props.loading &&
						<h2>Loading...</h2>
					}
					{ !this.props.loading &&
						<div
							style={{
								width: '100vw'
							}}
						>
							<h2>Share Link</h2>
							<SelectField
								style={{
									margin: '0 auto',
									display: 'inline-block'
								}}
								value={this.state.selectedHoneyJar}
								onChange={this.handleSelectHoneyJar}
								maxHeight={200}
								floatingLabelText="Honey Jar"
							>
								{honeyJars}
							</SelectField>
							<TextField
								style={{
									margin: '0 auto',
									display: 'inline-block'
								}}
								floatingLabelText="url"
								value={this.state.url}
								onChange={this.onChangeUrl}
								type="text"
							>
							</TextField>
							<RaisedButton
								label="Submit"
								style={{ 
									margin: "0 auto",
									width: 256 ,
									display: "inline-block",
									marginTop: 25
								}}
								backgroundColor="rgb(60, 177, 204)"
								labelColor="white"

								onTouchTap={this.handleSubmit}
							/>
						</div>
					}
				</Paper>
			</div>
		)
	}
}