import React from 'react';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router';
import { Meteor } from 'meteor/meteor';

export class Login extends React.Component {
	constructor(props) {
		super(props);
		let state = {
			emailErrorMessage: '',
			emailErrorStyle: {display:"hidden"},
			passwordErrorMessage: '',
			passwordErrorStyle: {display:"hidden"}
		};
		this.state = state;
		this.handleLogin = this.handleLogin.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
	}

	handleKeyDown(e) {
		if (e.key == 'Enter') {
			this.handleLogin();
		}
	}

	handleLogin() {
		let canLogin = true;
		if (!this.refs.email.getValue()) {
			this.setState({
				emailErrorStyle: {display:"inline-block"},
				emailErrorMessage: "Enter your email"
			});
			canLogin = false;
		} else {
			this.setState({
				emailErrorStyle: {display:"hidden"},
				emailErrorMessage: null
			});
		}
		if (!this.refs.password.getValue()) {
			this.setState({
				passwordErrorStyle: {display:"inline-block"},
				passwordErrorMessage: "Enter your password"
			});
			canLogin = false;
		} else {
			this.setState({
				passwordErrorStyle: {display:"hidden"},
				passwordErrorMessage: null
			});
		}
		if (canLogin) {
			Meteor.loginWithPassword(this.refs.email.getValue(), this.refs.password.getValue(), 
				(err) => {
					if (err) {
						this.props.handleError(err);
					} else {
						console.log('logged in');
						this.props.handleLogin();
					}
				}
			);
		}
	}

	render() {
		const paperStyle = {
			paddingTop: 10,
			paddingBottom: 60,
			height: "100%"
		}

		const inputStyle = {
			margin: "0 auto",
			display: "block",
			marginBottom: 20
		}

		return (
			<Paper style={paperStyle} zDepth={2}>
				<h2 style={{ textAlign: "center" }}>Welcome Back</h2>
				<TextField
					floatingLabelText="Email"
					ref="email"
					type="email"
					style={inputStyle}
					errorStyle={this.state.emailErrorStyle}
					errorText={this.state.emailErrorMessage}
					onKeyDown={this.handleKeyDown}
				/>
				<TextField
					floatingLabelText="Password"
					ref="password"
					type="password"
					style={inputStyle}
					errorStyle={this.state.passwordErrorStyle}
					errorText={this.state.passwordErrorMessage}
					onKeyDown={this.handleKeyDown}
				/>
				<RaisedButton
					label="Login"
					style={{ 
						margin: "0 auto",
						width: 256 ,
						display: "block",
						marginTop: 25
					}}
					backgroundColor="rgb(60, 177, 204)"
					labelColor="white"

					onTouchTap={this.handleLogin}
				/>
				<div style={{ fontSize: 10, marginTop: 15, marginLeft: 70 }}>
					Need an account? 
					<Link 
						to="/register"
						style={{ 
							color: "#4ab9b1",
							marginLeft: 2
						}}
					>
						Register
					</Link>
				</div>
				
			</Paper>
		)
	}
}