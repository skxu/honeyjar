import React from 'react';

export class LoginButtons extends React.Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div id="loginButtons">
    		Login Buttons 
    	</div>
	  )
  }
}

LoginButtons.propTypes = {
  align: React.PropTypes.string,
}

LoginButtons.defaultProps = {
  align: 'right',
}