import React from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Toggle from 'material-ui/Toggle';
import { Gravatar } from '/imports/ui/components/Gravatar';

export class HoneyJarUsers extends React.Component {
	constructor(props) {
		super(props);
		this.handleAddUser = this.handleAddUser.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
	}

	handleAddUser() {
		let userEmail = this.refs.addEmail.getValue();
		let honeyJar = this.props.honeyJar;
		Meteor.call("honeyjars.addUser", honeyJar, userEmail,
			(error) => {
				if (error) {
					console.log(error);
					this.props.handleError(error);
				} else {
					console.log("added user to honeyjar");
				}
			}
		);
	}

	handleKeyDown(e) {
		if (e.key == 'Enter') {
			this.handleAddUser();
		}
	}

	render() {
		if (this.props.loading) {
			return (
				<div>Loading</div>
			)
		}

		users = [];
		this.props.honeyJar.users.forEach((user) => {
			users.push(
				<ListItem key={"user-"+user._id}
					primaryText={user.username}
					leftAvatar={
						<Gravatar 
		                    email={user.emails[0].address}
		                    style={{ borderRadius: "50%", display: "span", width:40, height:40 }}
		                />
					}
				/>
			);
		});
		return(
			<div
				style={{
					width: "calc(100% - 40px)",
					height: "100%",
					margin: "0 auto",
					padding: 20,
					overflowX: "auto"
				}}
			>
				<h1>Users</h1>
				<List>
					{users}
				</List>
				<TextField
					floatingLabelText="email"
					ref="addEmail"
					type="text"
					style={{
						width: "80%"
					}}
					onKeyDown={this.handleKeyDown}
				/>
				<FloatingActionButton 
					mini={true} 
					style={{  }}
					onTouchTap={this.handleAddUser}
				>
			      <ContentAdd />
			    </FloatingActionButton>
			</div>
		)
	}
}