import React from 'react';
import Paper from 'material-ui/Paper';
import Toggle from 'material-ui/Toggle';

export class FeedSettings extends React.Component {
	constructor(props) {
		super(props);
		
	}



	render() {
		return (
			<div
				style={{
					padding:15,
					textAlign: "left"
				}}
			>
				<h2 style={{textAlign:"center"}}>Settings</h2>
				<Toggle
					label="Show Read"
					toggled={this.props.showRead}
					onTouchTap={this.props.toggleShowRead}
					labelPosition="left"
				/>
				<Toggle
					label="Only Show Favorites"
					toggled={this.props.showOnlyFavorites}
					onTouchTap={this.props.toggleShowOnlyFavorites}
					labelPosition="left"
				/>
				<Toggle
					label="Only Show Saved"
					toggled={this.props.showOnlySaved}
					onTouchTap={this.props.toggleShowOnlySaved}
					labelPosition="left"
				/>

			</div>
		)
	}
}