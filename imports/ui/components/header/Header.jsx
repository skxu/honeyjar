import React from 'react';
import { Link } from 'react-router';
import { LoginButtons } from '/imports/ui/components/login/LoginButtons.jsx';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { Gravatar } from '/imports/ui/components/Gravatar';
import { Meteor } from 'meteor/meteor';

export class Header extends React.Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    Meteor.logout((error) => {
      if (error) {
        this.props.handleError(error);
      } else {
        this.props.handleLogout();
      }
    });
  }
  
  render() {
    return (
      <div>
        <AppBar
          style={{ 
            backgroundColor:"#328BA8"
          }}
          titleStyle={{
            fontFamily: "'Dancing Script', cursive",
            fontSize: 35,
            textDecoration: "none"
          }}
          title={
            <Link 
              to="/"
              style={{
                color: "white",
                textDecoration: "none"
              }}
            >
              Honey Jar
            </Link>
          }
          iconElementLeft={
            <Link to="/">
              <img 
                src="/img/honeyjar.png" 
                style={{ width: 45, height: 45, color: "white" }}
              />
            </Link>

          }
          iconElementRight={
            <div>
              {
                this.props.user && 
                <div>
                  <Gravatar 
                    email={this.props.user.emails[0].address}
                    style={{ borderRadius: "50%", display: "span" }}
                  />
                  <IconMenu
                    iconButtonElement={
                      <IconButton
                        iconStyle={{ 
                          width: 25,
                          height: 25
                        }} 
                      >
                        <MoreVertIcon
                          color="white"
                        />
                      </IconButton>
                    }
                    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                    targetOrigin={{horizontal: 'right', vertical: 'top'}}
                  >
                    <MenuItem primaryText="Settings" />
                    <MenuItem 
                      primaryText="Sign out"
                      onTouchTap={this.handleLogout}
                    />
                  </IconMenu>
                </div>
              }
              {!this.props.user &&
                <div>
                  <Link 
                    to="/login"
                    style={{
                      color: "white",
                      display: "block",
                      marginTop: 14,
                      marginRight: 15,
                      textDecoration: "none"
                    }}
                  >
                    Login
                  </Link>
                </div>
              }
            </div>
          }
        />
      </div>
    );
  }
}