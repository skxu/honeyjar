import React from 'react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import CheckCircleIcon from 'material-ui/svg-icons/action/check-circle';
import FavoriteBorderIcon from 'material-ui/svg-icons/action/favorite-border';
import FavoriteIcon from 'material-ui/svg-icons/action/favorite';
import HourglassEmptyIcon from 'material-ui/svg-icons/action/hourglass-empty';
import HourglassFullIcon from 'material-ui/svg-icons/action/hourglass-full';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {grey400, green400, pink400, blueGrey400, red400} from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

export class HoneyJar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showDeleteWarning: false,
			feedToDelete: null,
			url: ''
		}
		this.handleAddLink = this.handleAddLink.bind(this);
		this.handleDeleteFeed = this.handleDeleteFeed.bind(this);
		this.handleCloseDeleteWarning = this.handleCloseDeleteWarning.bind(this);
		this.handleMarkAsRead = this.handleMarkAsRead.bind(this);
		this.onChangeUrl = this.onChangeUrl.bind(this);
		this.onKeyDown = this.onKeyDown.bind(this);
	}

	handleAddLink() {
		let link = {
			honeyJarId: this.props.honeyJar._id,
			url: this.state.url,
			authorUserId: this.props.user._id
		};
		Meteor.call("links.insert", link, (error, result) => {
			if (error) {
				console.log(error);
				this.props.handleError(error);
			} else {
				console.log("added link: " + result);
				this.setState({
					url: ''
				});
			}
		});
	}

	onChangeUrl(event) {
		this.setState({
			url: event.target.value
		});
	}

	handleClickFeed(feed) {
		let url = feed.link.url;
		window.open(url, '_blank');
		let feedId = feed._id;
		if (!feed.read) {
			Meteor.call("feeds.toggleRead", feedId, true,
				(error, result) => {
					if (error) {
						console.log(error);
						this.props.handleError(error);
					}
				}
			);
		}
	}

	handleClickRead(feed) {
		Meteor.call("feeds.toggleRead", feed._id, !feed.read,
			(error, results) => {
				if (error) {
					console.log(error);
					this.props.handleError(error);
				}
			}
		);
	}

	handleClickFavorite(feed) {
		Meteor.call("feeds.toggleFavorite", feed._id, !feed.favorite,
			(error, results) => {
				if (error) {
					console.log(error);
					this.props.handleError(error);
				}
			}
		);
	}

	handleClickSave(feed) {
		Meteor.call("feeds.toggleSave", feed._id, !feed.saveForLater,
			(error, results) => {
				if (error) {
					console.log(error);
					this.props.handleError(error);
				}
			}
		);
	}

	handleClickDelete(feed) {
		this.setState({
			showDeleteWarning: true,
			feedToDelete: feed
		});
	}

	handleDeleteFeed() {
		Meteor.call("feeds.delete", this.state.feedToDelete, function(error) {
			if (error) {
				console.log(error);
				this.props.handleError(error);
			}
		});
		this.setState({
			showDeleteWarning: false,
			feedToDelete: null
		});
	}

	handleCloseDeleteWarning() {
		this.setState({
			showDeleteWarning: false,
			feedToDelete: null
		});
	}

	handleMarkAsRead() {
		if (this.state.feedToDelete && !this.state.feedToDelete.read) {
			this.handleClickRead(this.state.feedToDelete);
		}
		this.handleCloseDeleteWarning();
	}

	onKeyDown(e) {
		if (e.key == 'Enter') {
			this.handleAddLink();
		}
	}

	render() {
		if (this.props.loading) {
			return (
				<div>Loading</div>
			)
		}

		feedItems = [];
		this.props.feeds.forEach((feed) => {
			if (!this.props.showRead && feed.read) {
				return
			} else if (this.props.showOnlySaved && !feed.saveForLater) {
				return
			} else if (this.props.showOnlyFavorites && !feed.favorite) {
				return
			}

			feedItems.push(
				<div key={"feed-key-"+feed._id}>
					<ListItem
						leftAvatar={
							<Avatar
								href={feed.link.url}
								src={feed.link.thumbnailUrl}
								target="_blank"
								onTouchTap={this.handleClickFeed.bind(this, feed)}
							/>
						}
						primaryText={
							<a 
								href={feed.link.url}
								target="_blank"
								style={{
									textDecoration: "none",
									color: "#000"
								}}
								onTouchTap={this.handleClickFeed.bind(this, feed)}
							>
								<p style={{margin:0}}>{feed.link.title}</p>
								<p 
								  style={{
								  	color: "grey",
								  	fontSize: "0.5em",
								  	fontWeight: 100,
								  	marginTop: 1
								  }}
								>
									{feed.link.url} -- Submitted by {Meteor.users.findOne(feed.link.authorUserId).username}
								</p>
							</a>
						}
						secondaryText={
							<p>
								{feed.link.description}
							</p>
						}
						rightIconButton={
							<div>
								{ !this.props.isMobile &&
									<div>
										<IconButton
									    	touch={true}
									    	tooltip={(feed.read) ? "Mark Unread" : "Mark Read" }
									    	tooltipPosition="bottom-left"
									    	onTouchTap={this.handleClickRead.bind(this, feed)}
									    	style={{paddingLeft: 0, paddingRight: 0, width:22, height:22}}
									    	iconStyle={{width: 22, height: 22}}
									  	>
									    	<CheckCircleIcon color={(feed.read) ? green400 : grey400 } />
									  	</IconButton>
									  	<IconButton
									    	touch={true}
									    	tooltip={(feed.favorite) ? "Unfavorite" : "Favorite" }
									    	tooltipPosition="bottom-left"
									    	onTouchTap={this.handleClickFavorite.bind(this, feed)}
									    	style={{paddingLeft: 0, paddingRight: 0, width:22, height:22}}
									    	iconStyle={{width: 22, height: 22}}
									  	>
									    	{!feed.favorite && <FavoriteBorderIcon color={grey400} />}
									    	{feed.favorite && <FavoriteIcon color={pink400} />}
									  	</IconButton>
									  	<IconButton
									    	touch={true}
									    	tooltip={(feed.saveForLater) ? "Unsave" : "Save For Later" }
									    	tooltipPosition="bottom-left"
									    	onTouchTap={this.handleClickSave.bind(this, feed)}
									    	style={{paddingLeft: 0, paddingRight: 0, width:22, height:22}}
									    	iconStyle={{width: 22, height: 22}}
									  	>
									    	{feed.saveForLater && <HourglassFullIcon color={ blueGrey400 } />}
									    	{!feed.saveForLater && <HourglassEmptyIcon color={ grey400 } />}
									  	</IconButton>
									  	<IconButton
									  		touch={true}
									    	tooltip="Delete"
									    	tooltipPosition="bottom-left"
									    	onTouchTap={this.handleClickDelete.bind(this, feed)}
									    	style={{paddingLeft: 0, paddingRight: 0, width:22, height:22}}
									    	iconStyle={{width: 22, height: 22}}
									  	>
									  		<DeleteIcon color={ grey400 } />
									  	</IconButton>
								  	</div>
								}
								{ this.props.isMobile &&
									<IconMenu iconButtonElement={
										<IconButton
											touch={true}
											tooltip="more"
											tooltipPosition="bottom-left"
										>
											<MoreVertIcon color={grey400} />
										</IconButton>
									}>
										<MenuItem onTouchTap={this.handleClickRead.bind(this, feed)}>
											<CheckCircleIcon color={(feed.read) ? green400 : grey400 } />
											{(feed.read) ? ' Mark Unread' : ' Mark Read'}
										</MenuItem>
									    <MenuItem onTouchTap={this.handleClickFavorite.bind(this, feed)}>
									    	{!feed.favorite && <FavoriteBorderIcon color={grey400} />}
									    	{feed.favorite && <FavoriteIcon color={pink400} />}
									    	{(feed.favorite) ? ' Unfavorite' : ' Favorite'}
									    </MenuItem>
									    <MenuItem onTouchTap={this.handleClickSave.bind(this, feed)}>
									    	{feed.saveForLater && <HourglassFullIcon color={ blueGrey400 } />}
									    	{!feed.saveForLater && <HourglassEmptyIcon color={ grey400 } />}
									    	{(feed.saveForLater) ? ' Unsave' : ' Save'}
									    </MenuItem>
									    <MenuItem onTouchTap={this.handleClickDelete.bind(this, feed)}>
									    	<DeleteIcon color={ grey400 } />
									    	Delete
									    </MenuItem>
									</IconMenu>
								}
						  	</div>
						}
					/>
					<Divider inset={true} />
				</div>
			);
		});
		return(
			<Paper
				zDepth={1}
				style={{
					width: "100%",
					height: "100%",
					margin: "0 auto",
					padding: (this.props.isMobile) ? 0 : 20,
					overflow: "auto"
				}}
			>
				<h1 style={{ textAlign: "center" }}>
					{this.props.honeyJar.name}
				</h1>
				<h3>Reading List</h3>
				<TextField
					floatingLabelText="url"
					value={this.state.url}
					onChange={this.onChangeUrl}
					onKeyDown={this.onKeyDown}
					type="text"
				/>
				<RaisedButton
					label="Add"
					onTouchTap={this.handleAddLink}
				/>
				<List>
					{feedItems}
				</List>
				<Dialog
		          actions={[
		          	<FlatButton
			        	label="Cancel"
			        	primary={true}
			        	onTouchTap={this.handleCloseDeleteWarning}
			      	/>,
			      	<FlatButton
			      		label="Mark as Read"
			      		onTouchTap={this.handleMarkAsRead}
			      	/>,
			      	<FlatButton
			        	label="Delete"
			        	secondary={true}
			        	onTouchTap={this.handleDeleteFeed}
			      	/>
		          ]}
		          modal={false}
		          open={this.state.showDeleteWarning}
		          onRequestClose={this.handleCloseDeleteWarning}
		        >
		        	<p>Deleting a link will delete it for everyone.</p>
		        	<p>(Use 'mark as read' if you want to hide it for just yourself)</p>
		        </Dialog>
				
			</Paper>
		)
	}
}