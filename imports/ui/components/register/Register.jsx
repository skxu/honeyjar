import React from 'react';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router';
import { Accounts } from 'meteor/accounts-base';


export class Register extends React.Component {
	constructor(props) {
		super(props);
		let state = {
			emailErrorMessage: '',
			emailErrorStyle: {display:"hidden"},
			passwordErrorMessage: '',
			passwordErrorStyle: {display:"hidden"},
			usernameErrorMessage: '',
			usernameErrorStyle: {display:"hidden"}
		};
		this.state = state;
		this.handleRegister = this.handleRegister.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
	}

	handleKeyDown(e) {
		if (e.key == 'Enter') {
			this.handleRegister();
		}
	}

	handleRegister() {
		let canRegister = true;
		if (!this.refs.email.getValue()) {
			this.setState({
				emailErrorStyle: {display:"inline-block"},
				emailErrorMessage: "Enter your email"
			});
			canRegister = false;
		} else {
			this.setState({
				emailErrorStyle: {display:"hidden"},
				emailErrorMessage: null
			});
		}
		if (!this.refs.password.getValue()) {
			this.setState({
				passwordErrorStyle: {display:"inline-block"},
				passwordErrorMessage: "Enter your password"
			});
			canRegister = false;
		} else if (this.refs.password.getValue().length < 6) {
			this.setState({
				passwordErrorStyle: {display:"inline-block"},
				passwordErrorMessage: "Password needs minimum 6 characters"
			});
			canRegister = false;
		} else {
			this.setState({
				passwordErrorStyle: {display:"hidden"},
				passwordErrorMessage: null
			});
		}
		if (!this.refs.username.getValue()) {
			this.setState({
				usernameErrorStyle: {display:"inline-block"},
				usernameErrorMessage: "Enter your username"
			});
			canRegister = false;
		} else {
			this.setState({
				usernameErrorStyle: {display:"hidden"},
				usernameErrorMessage: null
			});
		}
		if (canRegister) {
			let options = {
				email: this.refs.email.getValue(),
				password: this.refs.password.getValue(),
				username: this.refs.username.getValue(),
				honeyjars: []
			};

			Accounts.createUser(options, 
				(err) => {
					if (err) {
						this.props.handleError(err);
					} else {
						console.log('created user');
						this.props.handleLogin();
					}
				}
			);
			
		}
	}

	render() {
		const paperStyle = {
			paddingTop: 10,
			paddingBottom: 60,
			height: "100%"
		}

		const inputStyle = {
			margin: "0 auto",
			display: "block",
			marginBottom: 20
		}

		return (
			<Paper style={paperStyle} zDepth={2}>
				<h2 style={{ textAlign: "center" }}>Register</h2>
				<TextField
					floatingLabelText="Username"
					ref="username"
					type="username"
					style={inputStyle}
					errorStyle={this.state.usernameErrorStyle}
					errorText={this.state.usernameErrorMessage}
					onKeyDown={this.handleKeyDown}
				/>
				<TextField
					floatingLabelText="Email"
					ref="email"
					type="email"
					style={inputStyle}
					errorStyle={this.state.emailErrorStyle}
					errorText={this.state.emailErrorMessage}
					onKeyDown={this.handleKeyDown}
				/>
				<TextField
					floatingLabelText="Password"
					ref="password"
					type="password"
					style={inputStyle}
					errorStyle={this.state.passwordErrorStyle}
					errorText={this.state.passwordErrorMessage}
					onKeyDown={this.handleKeyDown}
				/>
				<RaisedButton
					label="Continue"
					style={{ 
						margin: "0 auto",
						width: 256 ,
						display: "block",
						marginTop: 25
					}}
					backgroundColor="rgb(60, 177, 204)"
					labelColor="white"
					onTouchTap={this.handleRegister}
				/>
				<div style={{ fontSize: 10, marginTop: 15, marginLeft: 70 }}>
					Already have an account? 
					<Link 
						to="/login"
						style={{ 
							color: "#4ab9b1",
							marginLeft: 2
						}}
					>
						Login
					</Link>
				</div>
			</Paper>
		)
	}
}