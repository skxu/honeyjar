import React from 'react';
import TextField from 'material-ui/TextField';
import { HoneyJars } from '/imports/api/honeyjars/honeyjars';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Meteor } from 'meteor/meteor';
import Paper from 'material-ui/Paper';
import {List, ListItem} from 'material-ui/List';
import {browserHistory} from 'react-router';

export class HoneyJarList extends React.Component {
	constructor(props) {
		super(props);
		this.handleCreateHoneyJar = this.handleCreateHoneyJar.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
	}

	handleCreateHoneyJar() {
		let honeyjar = {
			"name": this.refs.name.getValue(),
			"users": [this.props.user]
		}
		console.log(this.props.user._id);
		Meteor.call('honeyjars.insert', honeyjar, this.props.user._id, (error, result) => {
			if (error) {
				console.log(error);
				this.props.handleError(error);
			} else {
				console.log("created honeyjar: " + result);
			}
		});
	}

	handleClickHoneyJar(honeyJarId) {
		this.props.onSelectJar();
		browserHistory.push('/honeyjars/' + honeyJarId);
	}

	handleKeyDown(e) {
		if (e.key == 'Enter') {
			this.handleCreateHoneyJar();
		}
	}

	render() {
		var jars = [];;
		if (!this.props.loading) {
			for (let idx in this.props.honeyJars) {
				let jar = this.props.honeyJars[idx];
				jars.push(
					<ListItem 
						key={'honeyjar'+jar._id}
						primaryText={jar.name}
						onTouchTap={this.handleClickHoneyJar.bind(this, jar._id)}
					/>
				);
			}
		}
		return(
			<Paper style={{paddingTop: 20, height: "100%"}}>
				<h1>HoneyJars</h1>
				<List>
					{jars}
				</List>
				<TextField
					floatingLabelText="name"
					ref="name"
					type="text"
					style={{
						width: "80%"
					}}
					onKeyDown={this.handleKeyDown}
				/>
				<FloatingActionButton 
					mini={true} 
					style={{  }}
					onTouchTap={this.handleCreateHoneyJar}
				>
			      <ContentAdd />
			    </FloatingActionButton>
			</Paper>
		)
	}

}