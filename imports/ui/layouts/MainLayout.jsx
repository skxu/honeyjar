import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Snackbar from 'material-ui/Snackbar';
import { browserHistory } from 'react-router';

export class MainLayout extends React.Component {
  constructor(props) {
    super(props);
    let state = {
      showMessage: false,
      message: '',
      messageBodyStyle: {
        backgroundColor: "#000"  
      },
      autoHideDuration: -1
    };
    this.state = state;
    this.handleError = this.handleError.bind(this);
    this.handleCloseMessage = this.handleCloseMessage.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }


  handleError(error) {
    this.setState({ 
      showMessage:true,
      message:error.reason,
      messageBodyStyle: {
        backgroundColor: "#FF6363"
      },
      autoHideDuration: -1
    });
  }

  handleCloseMessage() {
    this.setState({ showMessage:false });
  }

  handleLogin() {
    browserHistory.push('/');
    this.setState({
      showMessage: true,
      message: this.props.message,
      messageBodyStyle: {
        backgroundColor: "#000"
      },
      autoHideDuration: 1500
    });
  }

  handleLogout() {
    browserHistory.push('/login');
    this.setState({
      showMessage: true,
      message: this.props.message,
      messageBodyStyle: {
        backgroundColor: "#000"
      },
      autoHideDuration: 1500
    });
  }


  render() {
  	const { main, header } = this.props;
    return (
    	<MuiThemeProvider>
        <div>
  	    	<div>
  	        	{header && React.cloneElement(
                header, 
                {
                  handleError: this.handleError,
                  handleLogout: this.handleLogout
                })}
  	        	{React.cloneElement(
                main, 
                {
                  handleError: this.handleError,
                  handleLogin: this.handleLogin
                })}
  	    	</div>
          <Snackbar
            open={this.state.showMessage}
            message={this.state.message}
            autoHideDuration={this.state.autoHideDuration}
            bodyStyle={this.state.messageBodyStyle}
            onRequestClose={this.handleCloseMessage}
          />
        </div>
	    </MuiThemeProvider>
    );
  }
}