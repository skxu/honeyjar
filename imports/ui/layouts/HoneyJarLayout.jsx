import React from 'react';

import Paper from 'material-ui/Paper';
import { HoneyJarList } from '/imports/ui/components/HoneyJarList';
import { HoneyJar } from '/imports/ui/components/HoneyJar';
import { HoneyJarUsers } from '/imports/ui/components/HoneyJarUsers';
import { FeedSettings } from '/imports/ui/components/FeedSettings';
import Divider from 'material-ui/Divider';
import MobileDetect from 'mobile-detect';
import {Tabs, Tab} from 'material-ui/Tabs';
import ListIcon from 'material-ui/svg-icons/action/list';
import JarIcon from 'material-ui/svg-icons/places/free-breakfast';
import SettingsIcon from 'material-ui/svg-icons/action/settings';


export class HoneyJarLayout extends React.Component {
	constructor(props) {
		super(props);
		const queryParams = this.props.location.query;
		let state = {
			showRead: true,
			showOnlyFavorites: false,
			showOnlySaved: false,
			mobileNavValue: (queryParams && queryParams.initialView) ?
				queryParams.initialView
				: "list"
		}
		this.state = state;
		this.toggleShowRead = this.toggleShowRead.bind(this);
		this.toggleShowOnlyFavorites = this.toggleShowOnlyFavorites.bind(this);
		this.toggleShowOnlySaved = this.toggleShowOnlySaved.bind(this);
		this.handleMobileNav = this.handleMobileNav.bind(this);
		this.onSelectJar = this.onSelectJar.bind(this);

	}

	toggleShowRead() {
		this.setState({
			showRead: !this.state.showRead
		});
	}

	toggleShowOnlyFavorites() {
		this.setState({
			showOnlyFavorites: !this.state.showOnlyFavorites
		});
	}

	toggleShowOnlySaved() {
		this.setState({
			showOnlySaved: !this.state.showOnlySaved
		});
	}

	handleMobileNav(value) {
		this.setState({
			mobileNavValue: value
		});
	}

	onSelectJar() {
		this.setState({
			mobileNavValue: "jar"
		});
	}

	render() {
		let md = new MobileDetect(navigator.userAgent);
		if (this.props.loading) {
			return (
				<div>Loading</div>
			)
		}
		return (
			<div>
				{md.mobile() &&
					<Tabs 
						value={this.state.mobileNavValue}
						onChange={this.handleMobileNav}
					>
						<Tab 
							value="list"
							icon={<ListIcon />}
						/>
						<Tab
							value="jar"
							icon={<JarIcon />}
						/>
						<Tab
							value="settings"
							icon={<SettingsIcon />}
						/>
					</Tabs>
				}

				<div
				  style={{
				  	width: "100vw",
				  	height: (md.mobile()) ? "calc(100vh - 117px)" : "calc(100vh - 69px)",
				  	display: "flex",
				  	textAlign: "center",
				  	overflow: "auto",
				  	backgroundColor: "white",
				  }}
				>
					{ (!md.mobile() || this.state.mobileNavValue == "list") &&
						<div
							style={{
								width: (md.mobile()) ? "100%" : "20%"
							}}
						>
							<HoneyJarList
								honeyJars={(this.props.userProfile) ? this.props.userProfile.honeyjars : null} 
								user={this.props.user}
								onSelectJar={this.onSelectJar}
								handleError={this.props.handleError}
							/>
						</div>
					}
					{ (!md.mobile() || this.state.mobileNavValue == "jar") &&
						<div
							style={{
								width: (md.mobile()) ? "100%" : "60%",
								height: "calc(100vh - 69px)",
								overflow: "auto"
							}}
						>
							{this.props.currentHoneyJar && 
								<HoneyJar 
									loading={this.props.loading}
									honeyJar={this.props.currentHoneyJar}
									feeds={this.props.feeds}
									user={this.props.user}
									showRead={this.state.showRead}
									showOnlyFavorites={this.state.showOnlyFavorites}
									showOnlySaved={this.state.showOnlySaved}
									handleError={this.props.handleError}
									isMobile={md.mobile()}
								/>
							}

						</div>
					}
					{ (!md.mobile() || this.state.mobileNavValue == "settings") && 
						<div
							style={{
								width: (md.mobile()) ? "100%" : "20%",
								backgroundColor: "white"
							}}
						>
							{this.props.currentHoneyJar && 
								<Paper>
									<FeedSettings
										showRead={this.state.showRead}
										showOnlyFavorites={this.state.showOnlyFavorites}
										showOnlySaved={this.state.showOnlySaved}
										toggleShowRead={this.toggleShowRead}
										toggleShowOnlySaved={this.toggleShowOnlySaved}
										toggleShowOnlyFavorites={this.toggleShowOnlyFavorites}
									/>
									<Divider inset={false}/>
									<HoneyJarUsers 
										honeyJar={this.props.currentHoneyJar}
										loading={this.props.loading}
										handleError={this.props.handleError}
									/>
								</Paper>
							}
						</div>
					}
				</div>
				
			</div>
		)
	}
}