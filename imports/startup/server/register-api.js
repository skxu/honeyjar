// Register your apis here

import '/imports/api/links/links';
import '/imports/api/links/server/publications';
import '/imports/api/links/methods';

import '/imports/api/honeyjars/honeyjars';
import '/imports/api/honeyjars/server/publications';
import '/imports/api/honeyjars/methods';

import '/imports/api/users/users';
import '/imports/api/users/server/publications';
import '/imports/api/users/methods';

import '/imports/api/profiles/profiles';
import '/imports/api/profiles/server/publications';

import '/imports/api/feeds/feeds';
import '/imports/api/feeds/server/publications';
import '/imports/api/feeds/methods';
