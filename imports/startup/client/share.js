import util from 'util';
import { browserHistory } from 'react-router';
import URI from 'urijs';

document.addEventListener('deviceReady', function() {
    window.plugins.intent.setNewIntentHandler(function (intent) {
        console.log(util.inspect(intent));
        let title = intent.clipItems[0].text;
        let url = URI.withinString(title, function(url) {
        	browserHistory.push('/share?url=' + url + '&title=' + title );
        });
    });
});