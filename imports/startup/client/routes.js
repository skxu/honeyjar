import React from 'react';
import { render } from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Router, Route, IndexRedirect, IndexRoute, browserHistory } from 'react-router';
import { Session } from 'meteor/session';
import MainLayoutContainer from '/imports/ui/containers/MainLayoutContainer';
import HeaderContainer from '/imports/ui/containers/HeaderContainer';
import { NotFound } from '/imports/ui/pages/not-found/NotFound';
import RegisterLoginContainer from '/imports/ui/containers/RegisterLoginContainer';
import HoneyJarContainer from '/imports/ui/containers/HoneyJarContainer';
import HoneyJarLayoutContainer from '/imports/ui/containers/HoneyJarLayoutContainer';
import ShareLinkContainer from '/imports/ui/containers/ShareLinkContainer';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

Session.setDefault('pathname', '/');
browserHistory.listen(function(event) {
  Session.set('pathname', event.pathname);
});

Meteor.startup(() => {
	// code to run on server at startup
   	render(
     	<Router history={ browserHistory }>
        <Route path="/" component={MainLayoutContainer}>
  				<IndexRoute name="index" components = {{ main: HoneyJarLayoutContainer, header: HeaderContainer }}/>
  				<Route name="register" path="register" components = {{ main: RegisterLoginContainer, header: HeaderContainer }}/>
  				<Route name="login" path="login" components = {{ main: RegisterLoginContainer, header: HeaderContainer }}/>
          <Route name="honeyjardetail" path="honeyjars/:id" components = {{ main: HoneyJarLayoutContainer, header: HeaderContainer }}/>
          <Route name="honeyjarlist" path="honeyjars" components = {{ main: HoneyJarLayoutContainer, header: HeaderContainer}}/>
          <Route name="sharelink" path="share" components = {{ main: ShareLinkContainer, header: HeaderContainer }}/>
  				<Route path="*" components = {{ main: NotFound, header: HeaderContainer }}/>
			  </Route>
      </Router>,
      	document.getElementById('app')
   );
});

