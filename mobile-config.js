// This section sets up some basic app metadata,
// the entire section is optional.
App.info({
  id: 'io.code.honeyjar',
  name: 'honeyjar',
  description: 'Share sweet stuff',
  author: 'Sam Xu',
  email: 'samkxu@gmail.com',
  website: 'http://honeyjar.code.io'
});

App.icons({
  // Android
  // 'android_ldpi': 'resources/icons/icon_android_ldpi-01.png',
  'android_mdpi': 'resources/android_mdpi.icon.png',
  'android_hdpi': 'resources/android_hdpi.icon.png',
  'android_xhdpi': 'resources/android_xhdpi.icon.png',
  'android_xxhdpi': 'resources/android_xxhdpi.icon.png',
  'android_xxxhdpi': 'resources/android_xxxhdpi.icon.png'
});

App.launchScreens({
  // Android
  //'android_ldpi_portrait': 'resources/splash/launch_android_ldpi_portrait-01.png',
  //'android_ldpi_landscape': 'resources/splash/launch_android_ldpi_landscape-01.png',
  'android_mdpi_portrait': 'resources/android_mdpi_portrait.splash.png',
  'android_mdpi_landscape': 'resources/android_mdpi_landscape.splash.png',
  'android_hdpi_portrait': 'resources/android_hdpi_portrait.splash.png',
  'android_hdpi_landscape': 'resources/android_hdpi_landscape.splash.png',
  'android_xhdpi_portrait': 'resources/android_xhdpi_portrait.splash.png',
  'android_xhdpi_landscape': 'resources/android_xhdpi_landscape.splash.png',
  'android_xxhdpi_portrait': 'resources/android_xxhdpi_portrait.splash.png',
  'android_xxhdpi_landscape': 'resources/android_xxhdpi_landscape.splash.png'}
);


// // Set PhoneGap/Cordova preferences
// App.setPreference('BackgroundColor', '0xff0000ff');
// App.setPreference('HideKeyboardFormAccessoryBar', true);
// App.setPreference('Orientation', 'default');
// App.setPreference('Orientation', 'all', 'ios');


// // Pass preferences for a particular PhoneGap/Cordova plugin
// App.configurePlugin('com.phonegap.plugins.facebookconnect', {
//   APP_ID: '1234567890',
//   API_KEY: 'supersecretapikey'
// });


// // Add custom tags for a particular PhoneGap/Cordova plugin
// // to the end of generated config.xml.
// // Universal Links is shown as an example here.
// App.appendToConfig(`
//   <universal-links>
//     <host name="localhost:3000" />
//   </universal-links>
// `);

App.setPreference("AndroidLaunchMode", "singleTask");
App.setPreference("AutoHideSplashScreen", "true");
App.setPreference("SplashScreenDelay", "2500");

// so external images can be displayed
App.accessRule("*");